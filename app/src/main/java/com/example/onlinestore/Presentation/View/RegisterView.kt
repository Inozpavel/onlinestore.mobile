package com.example.onlinestore.Presentation.View

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import com.example.onlinestore.Presentation.ViewModels.RegisterViewModel
import com.example.onlinestore.R
import com.example.onlinestore.databinding.FragmentRegisterBinding
import kotlinx.coroutines.launch

class RegisterView : Fragment() {

    private lateinit var viewModel: RegisterViewModel
    private lateinit var binding: FragmentRegisterBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentRegisterBinding.inflate(
            layoutInflater, container, false
        )
        viewModel = ViewModelProvider(this).get(
            RegisterViewModel::class.java
        )

        binding.buttonRegister.setOnClickListener {
            if (binding.editPasswordConfirmation.text.toString() != binding.editPassword.text.toString()) {
                Toast.makeText(
                    context,
                    "Пароли должены совпадать!",
                    Toast.LENGTH_LONG
                ).show()
                return@setOnClickListener
            }

            viewLifecycleOwner.lifecycleScope.launch {
                val result = viewModel.tryRegister(
                    binding.editEmail.text.toString(),
                    binding.editUsername.text.toString(),
                    binding.editPassword.text.toString(),
                    binding.editPasswordConfirmation.text.toString()
                )

                if (!result.isSuccess) {
                    Toast.makeText(context, result.message, Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(context, "Регистрация прошла успешно!", Toast.LENGTH_SHORT)
                        .show()
                    Navigation.findNavController(binding.root).navigate(R.id.navigation_login)
                }
            }

        }
        return binding.root
    }

}