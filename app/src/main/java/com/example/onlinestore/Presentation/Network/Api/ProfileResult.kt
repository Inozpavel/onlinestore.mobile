package com.example.onlinestore.Presentation.Network.Api

import com.example.onlinestore.Domain.Model.Profile

class ProfileResult(val isSuccess: Boolean, val profile: Profile?)