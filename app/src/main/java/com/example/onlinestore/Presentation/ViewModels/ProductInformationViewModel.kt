package com.example.onlinestore.Presentation.ViewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.onlinestore.Domain.Model.Product
import com.example.onlinestore.Presentation.Network.Api.Services.ApiProductsService

class ProductInformationViewModel : ViewModel() {

    private val apiService: ApiProductsService = ApiProductsService()

    private val _data: MutableLiveData<Product> = MutableLiveData()
    val data: LiveData<Product> = _data
    lateinit var product: Product

    suspend fun loadProduct(id: String) {
        val data = apiService.getProduct(id)
        _data.postValue(data)
    }
}