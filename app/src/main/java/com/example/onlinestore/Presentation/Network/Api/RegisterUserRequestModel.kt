package com.example.onlinestore.Presentation.Network.Api

class RegisterUserRequestModel(
    val email: String,
    val password: String,
    val passwordConfirmation: String,
    val username: String
)