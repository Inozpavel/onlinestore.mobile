package com.example.onlinestore.Presentation.View.Adapters

import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.onlinestore.Domain.Model.Product
import com.example.onlinestore.Presentation.ViewModels.ProductInformationViewModel
import com.example.onlinestore.R
import com.example.onlinestore.databinding.ProductLikedElementBinding
import com.google.gson.Gson

class LikedProductsListAdapter(private val data: List<Product>) :
    RecyclerView.Adapter<LikedProductsListAdapter.LikedProductViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LikedProductViewHolder {
        val binding =
            ProductLikedElementBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return LikedProductViewHolder(binding)
    }

    override fun onBindViewHolder(holder: LikedProductViewHolder, position: Int) {
        val product = data[position]
        holder.binding.productName.text = product.name
        holder.binding.productDescription.text = product.description
        holder.binding.productPrice.text = product.cost.toString()
        val image = product.image
        if (image != null) {
            val gson = Gson()
            val bytes = gson.fromJson(image, ByteArray::class.java)
            val bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.size)
            holder.binding.imageViewLikedProduct.setImageBitmap(bitmap)
        } else {
            holder.binding.imageViewLikedProduct.setImageResource(R.drawable.nophoto)
        }
        val viewModel = ProductInformationViewModel()
        viewModel.product = product
        holder.viewModel = viewModel
    }

    override fun getItemCount(): Int {
        return data.size
    }

    class LikedProductViewHolder(var binding: ProductLikedElementBinding) : RecyclerView.ViewHolder(
        binding.root
    ) {
        var viewModel: ProductInformationViewModel? = null
    }
}