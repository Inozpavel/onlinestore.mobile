package com.example.onlinestore.Presentation.View.Adapters

import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.onlinestore.Domain.Model.Product
import com.example.onlinestore.Presentation.ViewModels.ProductInformationViewModel
import com.example.onlinestore.R
import com.example.onlinestore.databinding.ProductElementBinding
import com.example.onlinestore.Presentation.View.Adapters.ProductsListAdapter.ProductViewHolder
import com.google.gson.Gson

class ProductsListAdapter(
    private val data: List<Product>,
    private val onClickListener: OnProductClickListener
) : RecyclerView.Adapter<ProductViewHolder>() {
    interface OnProductClickListener {
        fun onProductClick(product: Product?)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val binding =
            ProductElementBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ProductViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        val product = data[position]
        holder.binding.productName.text = product.name
        holder.binding.productPrice.text = "${product.cost} ₽"
        val image = product.image
        if (image != null) {
            val gson = Gson()
            val bytes = gson.fromJson(image, ByteArray::class.java)
            val bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.size)
            holder.binding.imageViewProduct.setImageBitmap(bitmap)
        } else {
            holder.binding.imageViewProduct.setImageResource(R.drawable.nophoto)
        }
        holder.itemView.setOnClickListener { v: View? -> onClickListener.onProductClick(product) }
        if (product.isLiked) {
            holder.binding.likeButton.setImageResource(R.drawable.heart_filled)
        } else {
            holder.binding.likeButton.setImageResource(R.drawable.heart)
        }
        val viewModel = ProductInformationViewModel()
        viewModel.product = product
        holder.viewModel = viewModel
        holder.binding.likeButton.setOnClickListener {
            val viewModelProduct = holder.viewModel!!.product!!
            viewModelProduct.isLiked = !viewModelProduct.isLiked
            if (viewModelProduct.isLiked) {
                holder.binding.likeButton.setImageResource(R.drawable.heart_filled)
            } else {
                holder.binding.likeButton.setImageResource(R.drawable.heart)
            }
        }
    }

    override fun getItemCount(): Int {
        return data.size
    }

    class ProductViewHolder(var binding: ProductElementBinding) : RecyclerView.ViewHolder(
        binding.root
    ) {
        var viewModel: ProductInformationViewModel? = null
    }
}