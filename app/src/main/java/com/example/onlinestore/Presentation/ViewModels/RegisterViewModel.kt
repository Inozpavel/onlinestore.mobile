package com.example.onlinestore.Presentation.ViewModels

import androidx.lifecycle.ViewModel
import com.example.onlinestore.Presentation.Network.Api.RegisterResult
import com.example.onlinestore.Presentation.Network.Api.RegisterUserRequestModel
import com.example.onlinestore.Presentation.Network.Api.Services.ApiProductsService

class RegisterViewModel : ViewModel() {
    private val apiService: ApiProductsService = ApiProductsService()

    suspend fun tryRegister(
        email: String,
        username: String,
        password: String,
        passwordConfirmation: String
    ): RegisterResult {
        val requestModel = RegisterUserRequestModel(email, username, password, passwordConfirmation)
        return apiService.registerUser(requestModel)
    }
}