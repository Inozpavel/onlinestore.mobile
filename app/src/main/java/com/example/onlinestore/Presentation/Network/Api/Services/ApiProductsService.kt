package com.example.onlinestore.Presentation.Network.Api.Services

import com.example.onlinestore.Domain.Model.Product
import com.example.onlinestore.Presentation.Network.Api.*
import com.google.gson.GsonBuilder
import retrofit2.HttpException
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ApiProductsService {
    private var storeApi: IOnlineStoreApi

    init {
        val retrofit = Retrofit
            .Builder()
            .baseUrl("http://10.0.2.2:10000")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        storeApi = retrofit.create(IOnlineStoreApi::class.java)
    }

    suspend fun getProductsList(query: String?, skip: Int, take: Int): List<Product>? =
        try {
            storeApi.getProductsList(query, skip, take).items
        } catch (t: Throwable) {
            null
        }

    suspend fun getProduct(id: String): Product? =
        try {
            storeApi.getProduct(id)
        } catch (t: Throwable) {
            null
        }

    suspend fun getProfile(token: String?): ProfileResult =
        try {
            val tokenValue = "Bearer $token"
            ProfileResult(true, storeApi.getProfile(tokenValue))
        } catch (t: HttpException) {
            val text = t.response()?.errorBody()?.string().toString()
            ProfileResult(false, null)
        } catch (t: Throwable) {
            ProfileResult(false, null)
        }

    suspend fun registerUser(
        requestModel: RegisterUserRequestModel
    ): RegisterResult =
        try {
            storeApi.registerUser(requestModel)
            RegisterResult(true, "")
        } catch (t: HttpException) {
            val content = t.response()?.errorBody()?.string()
            val errorsContent = GsonBuilder()
                .create()
                .fromJson(content, ErrorResponse::class.java)
                .errors

            val message =
                errorsContent?.Title
                    ?: errorsContent?.Email?.firstOrNull()
                    ?: errorsContent?.Password?.firstOrNull()
                    ?: errorsContent?.Username?.firstOrNull()
                    ?: "Произошла ошибка! Попробуйте еще раз!"

            RegisterResult(false, message)
        }

    suspend fun getToken(requestModel: GetTokenRequestModel): GetTokenResult {
        return try {
            GetTokenResult(true, storeApi.getToken(requestModel))
        } catch (t: HttpException) {
            val text = t.response()?.errorBody()?.string().toString()
            GetTokenResult(false, null);
        }
        catch (t: Throwable) {
            GetTokenResult(false, null)
        }
    }
}