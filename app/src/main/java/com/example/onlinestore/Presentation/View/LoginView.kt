package com.example.onlinestore.Presentation.View

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import com.example.onlinestore.Domain.Model.Token
import com.example.onlinestore.Presentation.ViewModels.LoginViewModel
import com.example.onlinestore.R
import com.example.onlinestore.databinding.FragmentLoginBinding
import kotlinx.coroutines.launch
import java.time.OffsetDateTime

class LoginView : Fragment() {

    private lateinit var viewModel: LoginViewModel
    private lateinit var binding: FragmentLoginBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentLoginBinding.inflate(
            layoutInflater, container, false
        )
        viewModel = ViewModelProvider(this).get(
            LoginViewModel::class.java
        )

        binding.buttonLogin.setOnClickListener {
            viewLifecycleOwner.lifecycleScope.launch {
                val result = viewModel.getToken(
                    binding.editEmail.text.toString(),
                    binding.editPassword.text.toString()
                )
                if (!result.isSuccess) {
                    Toast.makeText(
                        context,
                        "Произошла ошибка, попробуйте войти еще раз!",
                        Toast.LENGTH_SHORT
                    ).show()
                    Navigation.findNavController(binding.root).navigate(R.id.navigation_login)
                    return@launch
                }
                val preferences = requireActivity().getSharedPreferences(
                    ProfileView.sharedPreferencesKey,
                    Context.MODE_PRIVATE
                )

                preferences
                    .edit()
                    .putString(ProfileView.tokenKey, result.token?.token)
                    .apply()

                Navigation.findNavController(binding.root).navigate(R.id.navigation_profile_details)
            }
        }

        binding.buttonRegister.setOnClickListener {
            Navigation.findNavController(binding.root).navigate(R.id.navigation_register)
        }

        return binding.root
    }

}