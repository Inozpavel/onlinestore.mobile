package com.example.onlinestore.Presentation.ViewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.onlinestore.Domain.Model.Product
import com.example.onlinestore.Presentation.Network.Api.Services.ApiProductsService

class AllProductsViewModel : ViewModel() {

    private val apiService: ApiProductsService = ApiProductsService()

    private val products: MutableList<Product> = mutableListOf()

    private val _data: MutableLiveData<List<Product>> = MutableLiveData()
    val data: LiveData<List<Product>> = _data

    suspend fun loadProducts(query: String? = null) {
        val data = apiService.getProductsList(query, 0, 20)
        _data.postValue(data)
    }

    fun setIsLiked(index: Int, isLiked: Boolean) {

        products[index].isLiked = isLiked
        _data.value = products
    }
}