package com.example.onlinestore.Presentation.Network.Api

import com.example.onlinestore.Domain.Model.CollectionModel
import com.example.onlinestore.Domain.Model.Product
import com.example.onlinestore.Domain.Model.Profile
import com.example.onlinestore.Domain.Model.Token
import retrofit2.http.*
import java.util.*

interface IOnlineStoreApi {

    @GET("api/products")
    @Headers("Accept: application/json")
    suspend fun getProductsList(
        @Query("query") query: String?,
        @Query("skip") skip: Int,
        @Query("take") take: Int,
    ): CollectionModel<Product>

    @GET("api/products/{id}")
    @Headers("Accept: application/json")
    suspend fun getProduct(@Path("id") id: String): Product

    @GET("api/users/current/profile")
    @Headers("Accept: application/json")
    suspend fun getProfile(@Header("Authorization") token: String): Profile

    @POST("api/users/token")
    @Headers("Accept: application/json")
    suspend fun getToken(@Body requestModel: GetTokenRequestModel): Token

    @POST("api/users/register")
    @Headers("Accept: application/json")
    suspend fun registerUser(@Body requestModel: RegisterUserRequestModel)
}