package com.example.onlinestore.Presentation.View

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.onlinestore.Domain.Model.Product
import com.example.onlinestore.databinding.FragmentProductsLikedBinding
import com.example.onlinestore.Presentation.View.Adapters.LikedProductsListAdapter
import com.example.onlinestore.Presentation.ViewModels.LikedProductsViewModel

class ProductsLikedView : Fragment() {
    private var binding: FragmentProductsLikedBinding? = null
    private lateinit var viewModel: LikedProductsViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        binding = FragmentProductsLikedBinding.inflate(
            layoutInflater, container, false
        )
        binding!!.productListRecycler.layoutManager = LinearLayoutManager(context)
        viewModel = ViewModelProvider(this).get(LikedProductsViewModel::class.java)
        viewModel.products.observe(
            viewLifecycleOwner
        ) { productsList: List<Product> ->
            binding!!.productListRecycler.adapter = LikedProductsListAdapter(
                productsList
            )
        }
        return binding!!.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}