package com.example.onlinestore.Presentation.View

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts.OpenDocument
import androidx.core.text.set
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import com.example.onlinestore.Domain.Model.Profile
import com.example.onlinestore.Presentation.Network.Api.ProfileResult
import com.example.onlinestore.Presentation.ViewModels.ProfileDetailsViewModel
import com.example.onlinestore.R
import com.example.onlinestore.databinding.FragmentProfileDetailsBinding
import kotlinx.coroutines.launch
import java.time.OffsetDateTime

class ProfileDetailsView : Fragment() {

    private lateinit var binding: FragmentProfileDetailsBinding
    private lateinit var viewModel: ProfileDetailsViewModel

    private val userImageKey = "user_profile_image_uri"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        binding = FragmentProfileDetailsBinding.inflate(layoutInflater, container, false)
        viewModel = ViewModelProvider(this).get(ProfileDetailsViewModel::class.java)

        val preferences = requireActivity().getSharedPreferences(
            ProfileView.sharedPreferencesKey,
            Context.MODE_PRIVATE
        )

        val image = preferences.getString(userImageKey, null)
        if (image != null) {
            binding.imageViewUser.setImageURI(Uri.parse(image))
        } else {
            binding.imageViewUser.setImageResource(R.drawable.nophoto)
        }

        val token = preferences.getString(ProfileView.tokenKey, null)

        binding.buttonSaveChanges.visibility = View.INVISIBLE
        binding.buttonSaveChanges.setOnClickListener { buttonSaveClicked() }
        viewModel.user.observe(viewLifecycleOwner) { result: ProfileResult ->

            if (!result.isSuccess) {
                Toast.makeText(
                    context,
                    "Произошла ошибка, попробуйте войти еще раз!",
                    Toast.LENGTH_SHORT
                ).show()
                Navigation.findNavController(binding.root).navigate(R.id.navigation_login)
                return@observe
            }
            val profile = result.profile!!
            binding.textViewUsername.text = profile.username
            binding.editTextEmail.setText(profile.email)
            binding.textViewPhone.text = profile.phone
            binding.textViewDateOfBirth.text =
                OffsetDateTime.now().toLocalDate().minusYears(20).toString()
        }

        binding.imageViewUser.setOnClickListener { profileImageClicked() }
        binding.editTextEmail.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                val currentEmail = binding.editTextEmail.text.toString()
//                var profile = viewModel.user.value.profile
//                val currentSavedEmail = profile.email
//                if (currentSavedEmail != currentEmail) {
//                    binding.buttonSaveChanges.visibility = View.VISIBLE
//                } else {
//                    binding.buttonSaveChanges.visibility = View.INVISIBLE
//                }
            }

            override fun afterTextChanged(s: Editable) {}
        })

        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.loadProfile(token)
        }

        return binding.root
    }

    private fun buttonSaveClicked() {
//        val currentEmail = binding.editTextEmail.text.toString()
//        if (viewModel.validatedEmail != null && viewModel.validatedEmail == currentEmail) {
//            Toast.makeText(
//                activity,
//                "Почта некорректна, сначала измените ее!",
//                Toast.LENGTH_SHORT
//            ).show()
//            return
//    }
    }

    private fun profileImageClicked() {
        val mainActivity: Activity? = activity
        requireActivity().activityResultRegistry.register(
            "key",
            OpenDocument()
        ) { result: Uri? ->
            if (result == null) {
                return@register
            }
            mainActivity!!.applicationContext.contentResolver.takePersistableUriPermission(
                result,
                Intent.FLAG_GRANT_READ_URI_PERMISSION
            )
            val uri = Uri.parse(result.toString())
            binding.imageViewUser.setImageURI(uri)
            val sharedPreferences = activity
                ?.getSharedPreferences(ProfileView.sharedPreferencesKey, Context.MODE_PRIVATE)
            val editor = sharedPreferences?.edit()
            editor?.putString(userImageKey, result.toString())
            editor?.apply()
        }.launch(arrayOf("image/*"))
    }
}