package com.example.onlinestore.Presentation.Network.Api

class ErrorResponse(
    val errors: Errors?,
) {
    data class Errors(
        val Title: String?,
        val Email: List<String>?,
        val Password: List<String>?,
        val Username: List<String>?
    )
}