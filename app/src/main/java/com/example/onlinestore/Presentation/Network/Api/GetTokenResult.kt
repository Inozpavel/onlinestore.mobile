package com.example.onlinestore.Presentation.Network.Api;

import com.example.onlinestore.Domain.Model.Token

class GetTokenResult(val isSuccess: Boolean, val token: Token?)
