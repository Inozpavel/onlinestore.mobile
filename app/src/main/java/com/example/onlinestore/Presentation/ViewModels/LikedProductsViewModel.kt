package com.example.onlinestore.Presentation.ViewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.onlinestore.Domain.Model.Product
import com.example.onlinestore.Presentation.Network.Api.Services.ApiProductsService

class LikedProductsViewModel : ViewModel() {

    private val _products = MutableLiveData<List<Product>>()

    val products: LiveData<List<Product>>
        get() = _products
}