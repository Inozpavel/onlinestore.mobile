package com.example.onlinestore.Presentation.ViewModels

import androidx.lifecycle.ViewModel
import com.example.onlinestore.Presentation.Network.Api.GetTokenRequestModel
import com.example.onlinestore.Presentation.Network.Api.GetTokenResult
import com.example.onlinestore.Presentation.Network.Api.Services.ApiProductsService

class LoginViewModel : ViewModel() {

    private val apiService: ApiProductsService = ApiProductsService()

    suspend fun getToken(email: String, password: String): GetTokenResult {
        val requestModel = GetTokenRequestModel(email, password)
        return apiService.getToken(requestModel);
    }
}