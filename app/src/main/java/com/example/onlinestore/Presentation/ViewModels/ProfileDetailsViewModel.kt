package com.example.onlinestore.Presentation.ViewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.onlinestore.Domain.Model.Profile
import com.example.onlinestore.Presentation.Network.Api.ProfileResult
import com.example.onlinestore.Presentation.Network.Api.Services.ApiProductsService
import java.time.OffsetDateTime

class ProfileDetailsViewModel : ViewModel() {
    private val apiService: ApiProductsService = ApiProductsService()
    var data: MutableLiveData<ProfileResult> = MutableLiveData()
    val user: LiveData<ProfileResult>
        get() = data

    suspend fun loadProfile(token: String?) {
        data.postValue(apiService.getProfile(token))
    }
}