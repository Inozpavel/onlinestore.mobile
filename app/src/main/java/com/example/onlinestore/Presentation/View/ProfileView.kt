package com.example.onlinestore.Presentation.View

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import com.example.onlinestore.Presentation.ViewModels.ProfileDetailsViewModel
import com.example.onlinestore.R
import com.example.onlinestore.databinding.FragmentProfileBinding

class ProfileView : Fragment() {
    private lateinit var viewModel: ProfileDetailsViewModel
    private lateinit var binding: FragmentProfileBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentProfileBinding.inflate(layoutInflater, container, false)
        viewModel = ViewModelProvider(this).get(ProfileDetailsViewModel::class.java)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val preferences = requireActivity().getSharedPreferences(
            sharedPreferencesKey,
            Context.MODE_PRIVATE
        )

        val token = preferences.getString(tokenKey, null)

        if (token != null) {
            Navigation.findNavController(binding.root).navigate(R.id.navigation_profile_details)
        } else {
            Navigation.findNavController(binding.root).navigate(R.id.navigation_login)
        }
    }

    companion object {
        const val tokenKey: String = "onlineStore.user_token"
        const val sharedPreferencesKey = "com.example.onlinestore"
    }
}