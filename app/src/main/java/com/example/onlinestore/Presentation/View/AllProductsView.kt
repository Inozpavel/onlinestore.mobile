package com.example.onlinestore.Presentation.View

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.onlinestore.Domain.Model.Product
import com.example.onlinestore.Presentation.ViewModels.AllProductsViewModel
import com.example.onlinestore.R
import com.example.onlinestore.databinding.FragmentAllProductsBinding
import com.example.onlinestore.Presentation.View.Adapters.ProductsListAdapter
import kotlinx.coroutines.launch

class AllProductsView : Fragment() {
    private lateinit var viewModel: AllProductsViewModel
    private var binding: FragmentAllProductsBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentAllProductsBinding.inflate(inflater, container, false)
        binding!!.buttonShowAllProducts.setOnClickListener { x: View -> onClick(x) }
        binding!!.productListRecycler.layoutManager = LinearLayoutManager(context)
        viewModel = ViewModelProvider(this).get(AllProductsViewModel::class.java)
        val productClickListener = object : ProductsListAdapter.OnProductClickListener {
            override fun onProductClick(product: Product?) {
                val bundle = Bundle()
                bundle.putString("Id", product?.id.toString())
                Navigation.findNavController(
                    binding!!.root
                ).navigate(R.id.navigation_product_information, bundle)
            }
        }

        binding!!.searchViewProduct.setOnQueryTextListener(object : SearchView.OnQueryTextListener,
            androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(text: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(text: String?): Boolean {
                Log.e("onQueryTextChange", "Change")

                viewLifecycleOwner.lifecycleScope.launch {
                    viewModel.loadProducts(text)
                }
                return false
            }
        })

        viewModel.data.observe(viewLifecycleOwner) { productsList: List<Product> ->
            binding!!.productListRecycler.adapter = ProductsListAdapter(
                productsList,
                productClickListener
            )
        }

        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.loadProducts(null)
        }

        return binding!!.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

    companion object {
        private fun onClick(x: View) {

            Navigation.findNavController(x).navigate(R.id.navigation_product_information)
        }
    }
}