package com.example.onlinestore.Presentation.View

import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.example.onlinestore.Presentation.ViewModels.ProductInformationViewModel
import com.example.onlinestore.R
import com.example.onlinestore.databinding.FragmentProductInformationBinding
import com.google.gson.Gson
import kotlinx.coroutines.launch

class ProductInformationView : Fragment() {
    private lateinit var viewModel: ProductInformationViewModel
    private lateinit var binding: FragmentProductInformationBinding
    var isLiked: Boolean = false;

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentProductInformationBinding.inflate(
            layoutInflater, container, false
        )
        viewModel = ViewModelProvider(this).get(
            ProductInformationViewModel::class.java
        )

        viewModel.data.observe(viewLifecycleOwner) { product ->

            if (product == null) {
                return@observe
            }

            binding.productName.text = product.name
            binding.productPrice.text = "${product.cost} ₽";
            binding.productDescription.text = product.description;
            if (product.isLiked) {
                binding.likeButton.setImageResource(R.drawable.heart_filled)
            } else {
                binding.likeButton.setImageResource(R.drawable.heart)
            }
            val image = product.image
            if (image != null) {
                val gson = Gson()
                val bytes = gson.fromJson(image, ByteArray::class.java)
                val bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.size)
                binding.productImage.setImageBitmap(bitmap)
            } else {
                binding.productImage.setImageResource(R.drawable.nophoto)
            }
        }

        binding.likeButton.setOnClickListener {
            isLiked = !isLiked
            if (isLiked) {
                binding.likeButton.setImageResource(R.drawable.heart_filled)
            } else {
                binding.likeButton.setImageResource(R.drawable.heart)
            }
        }

        val id = arguments?.getString("Id")!!
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.loadProduct(id);
        }
        return binding.root
    }
}