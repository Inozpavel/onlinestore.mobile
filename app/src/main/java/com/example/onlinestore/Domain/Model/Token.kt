package com.example.onlinestore.Domain.Model

class Token(val token: String, val expiration: Int)