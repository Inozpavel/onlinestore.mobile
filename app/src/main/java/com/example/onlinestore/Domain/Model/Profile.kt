package com.example.onlinestore.Domain.Model

class Profile(
    var email: String,
    var username: String,
    var phone: String,
)