package com.example.onlinestore.Domain.Model

open class Product(
    val id: String,
    val name: String,
    val description: String?,
    val cost: Double,
    var isLiked: Boolean,
    val image: String?
)